const express = require("express");

// express.Router() method allows access to HTTP methods
const router = express.Router();

const taskControllers = require("../controllers/taskController");

// Create - task routes
router.post("/addTask", taskControllers.createTaskController);

// Get all tasks
router.get("/allTask", taskControllers.getAllTasksController);

// Delete a task
router.delete("/deleteTask/:taskId", taskControllers.deleteTaskController);

// Change the status of a task to "complete"
// This route expects to receive a put request at the URL "/tasks/:id/complete"
// The whole URL is at "http://localhost:3001/tasks/:id/complete"
// We cannot use put("/tasks/:id") again because it has already been used in our route to update a task
router.put("/:id/archive", (req, res) => {
	taskControllers.changeNameToArchive(req.params.id).then(resultFromController => res.send(resultFromController));
}) 

router.patch("/updateTask/:taskId", taskControllers.updateTaskNameController);


// Activity
// getSpecificTaskController
router.get("/:id", taskControllers.getSpecificTaskController);

// updateStatusController
router.put("/:id/complete", (req, res) => {
	taskControllers.updateStatusController(req.params.id).then(resultFromController => res.send(resultFromController));
})


module.exports = router;




